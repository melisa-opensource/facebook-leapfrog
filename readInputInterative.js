const rl = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
})

const readInput = (caseNumber, logic) => {
  return new Promise((resolve) => {
    rl.question('', function (answer) {
      // console.log(`problem: ${answer}`)
      let result = logic(answer) // run logic
      console.log(`Case #${caseNumber}: ${result}`)
      resolve()
    })
  })
}

module.exports = (logic) => {
  rl.question('', async (answer) => {
    const totalQuestions = parseInt(answer)
    for (let i = 0; i < totalQuestions; i++) {
      await readInput(i + 1, logic)
    }
    rl.close()
  })
}
