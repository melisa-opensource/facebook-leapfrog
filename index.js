const enviroment = require('./enviroment')
const DEBUG = enviroment.DEBUG
const log = enviroment.log
const INTERATIVE = enviroment.INTERATIVE
const FAKE = enviroment.FAKE

// enviroment prod
if (!DEBUG) {
  const readInput = require('./readInput')
  const logic = require('./logic')
  process.stdout.write(readInput(logic))
  return
}

// interative
if (INTERATIVE) {
  const readInput = require('./readInputInterative')
  const logic = require('./logic')
  readInput((caseLilypads) => {
    return logic(caseLilypads)
  })
  return
}

const cases = DEBUG ? FAKE ? require('./fakedata').split('\n').slice(1) : [
  'A.',
  'AB.',
  'ABB',
  'A.BB',
  'A..BB..B',
  'A.B..BBB.',
  'AB.........',
  'A.B..BBBB.BB'
] : null

if (DEBUG) {
  console.clear()
  const startTime = new Date()
  const logic = require('./logic')
  cases.forEach((c) => {
    logic(c)
  })
  const used = process.memoryUsage().heapUsed / 1024 / 1024
  log(`Uses approximately ${used} MB`)
  log('Execution time: %dms', new Date() - startTime)
  return
}
