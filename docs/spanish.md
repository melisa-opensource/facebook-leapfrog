# Leapfrog (Salto de Rana)

Una colonia de ranas reside pacíficamente en un estanque. La colonia está liderada por una sola rana alfa, y también incluye 0 o más ranas Beta. Para ser un buen líder, el Alpha Frog estudia diligentemente el arte de las fracciones todos los días.

Hay N lilypads en una fila en la superficie del estanque, numerados del 1 al N de izquierda a derecha, cada uno de los cuales es lo suficientemente grande como para que quepa una rana a la vez. Hoy en día, el Alpha Frog se encuentra en el lilypad de la izquierda, y debe abrirse paso hacia el lilypad de la derecha antes de que pueda comenzar su práctica de fracciones.

El estado inicial de cada lilypad i se describe mediante un carácter Li, que es uno de los siguientes:

"A": Ocupada por la rana alfa (se garantiza que Li = "A" si y solo si i = 1)
"B": ocupada por una rana beta
".": Desocupado

En cada punto en el tiempo, una de las siguientes cosas puede ocurrir:
1) La rana alfa puede saltar sobre uno o más lilypads inmediatamente a su derecha que están ocupados por ranas beta, y aterrizar en el siguiente lilypad desocupado, si existe tal lilypad. La rana alfa debe saltar sobre al menos una rana beta; no puede simplemente saltar a un lilypad adyacente. La rana alfa solo puede saltar a su derecha.

2) Cualquier rana Beta puede saltar al siguiente lilypad a su izquierda o derecha, si tal lilypad existe y está desocupado.
Suponiendo que todas las ranas cooperen, determine si es posible o no que la Rana alfa llegue al lilypad más a la derecha y comience su práctica diaria de fracciones.

## Entrada

La entrada comienza con un entero T, el número de días en que la rana alfa estudia las fracciones. Para cada día, hay una sola línea que contiene la cadena L de longitud-N.

## Salida

Para el último día, imprima una línea que contenga "Case #i:" seguido de un solo carácter: "Y" si la rana alfa puede alcanzar el lilypad más a la derecha, o "N" de lo contrario.

## Restricciones

1 ≤ T ≤ 500
2 ≤ N ≤ 5,000

## Explicación de la muestra

En el primer caso, la rana alfa no puede saltar a ningún lado.

En el segundo caso, la rana alfa puede saltar sobre la rana beta para alcanzar el lilypad más a la derecha.

En el tercer caso, ni la rana alfa ni ninguna de las ranas beta pueden saltar a ninguna parte.

En el cuarto caso, si la primera Beta Frog salta un lilypad a la izquierda, y luego la segunda 

Beta Frog también salta un lilypad a la izquierda, entonces la Alpha Frog puede saltar sobre ambos para alcanzar el lilypad más a la derecha.

| Ejemplo de entrada  | Ejemplo de salida  |
|---|---|
| 8  |   |
| A.  | Case #1: N  |
| AB.  | Case #2: Y  |
| ABB  | Case #3: N  |
| A.BB  | Case #4: Y  |
| A..BB..B  | Case #5: N  |
| A.B..BBB.  | Case #6: Y  |
| AB.........  | Case #7: N  |
| A.B..BBBB.BB  | Case #8: N  |