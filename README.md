# Leapfrog problem

**This problem statement differs from that of Leapfrog Ch. 2 in only one spot, highlighted in bold below.**

A colony of frogs peacefully resides in a pond. The colony is led by a single Alpha Frog, and also includes 0 or more Beta Frogs. In order to be a good leader, the Alpha Frog diligently studies the high art of fractions every day.

There are **N** lilypads in a row on the pond's surface, numbered 1 to **N** from left to right, each of which is large enough to fit at most one frog at a time. Today, the Alpha Frog finds itself on the leftmost lilypad, and must leap its way to the rightmost lilypad before it can begin its fractions practice.

The initial state of each lilypad i is described by a character Li, which is one of the following:

"A": Occupied by the Alpha Frog (it's guaranteed that Li = "A" if and only if i = 1)
"B": Occupied by a Beta Frog
".": Unoccupied
At each point in time, one of the following things may occur:

1) The Alpha Frog may leap over one or more lilypads immediately to its right which are occupied by Beta Frogs, and land on the next unoccupied lilypad past them, if such a lilypad exists. The Alpha Frog must leap over at least one Beta Frog; it may not just leap to an adjacent lilypad. **Note that, unlike in Leapfrog Ch. 2, the Alpha Frog may only leap to its right.**

2) Any Beta Frog may leap to the next lilypad to either its left or right, if such a lilypad exists and is unoccupied.

Assuming the frogs all cooperate, determine whether or not it's possible for the Alpha Frog to ever reach the rightmost lilypad and begin its daily fractions practice.

## Input
Input begins with an integer **T**, the number of days on which the Alpha Frog studies fractions. For each day, there is a single line containing the length-**N** string **L**.

## Output
For the ith day, print a line containing "Case #i: " followed by a single character: "Y" if the Alpha Frog can reach the rightmost lilypad, or "N" otherwise.

## Constraints
1 ≤ **T** ≤ 500

2 ≤ **N** ≤ 5,000 

## Explanation of Sample
In the first case, the Alpha Frog can't leap anywhere.

In the second case, the Alpha Frog can leap over the Beta Frog to reach the rightmost lilypad.

In the third case, neither the Alpha Frog nor either of the Beta Frogs can leap anywhere.

In the fourth case, if the first Beta Frog leaps one lilypad to the left, and then the second Beta Frog also leaps one lilypad to the left, then the Alpha Frog can leap over both of them to reach the rightmost lilypad.

| Sample input  | Sample output  |
|---|---|
| 8  |   |
| A.  | Case #1: N  |
| AB.  | Case #2: Y  |
| ABB  | Case #3: N  |
| A.BB  | Case #4: Y  |
| A..BB..B  | Case #5: N  |
| A.B..BBB.  | Case #6: Y  |
| AB.........  | Case #7: N  |
| A.B..BBBB.BB  | Case #8: Y  |

``` bash
8
A.
AB.
ABB
A.BB
A..BB..B
A.B..BBB.
AB.........
A.B..BBBB.BB
```

```bash
Case #1: N
Case #2: Y
Case #3: N
Case #4: Y
Case #5: N
Case #6: Y
Case #7: N
Case #8: Y
```

## Links

1. [Link problem](https://www.facebook.com/hackercup/problem/656203948152907/)
1. [Code](https://gitlab.com/melisa-opensource/facebook-leapfrog)
1. [Chat Group](https://chat.whatsapp.com/JVjTxoo1sGp0pPG6bfIXvw)
1. [Document problem](https://docs.google.com/document/d/1MxpEXVn3cCQ0UYliBKvlrbInqoQTkOshehi4TN_FErc/edit)
1. [Mardown table](https://www.tablesgenerator.com/markdown_tables)

# Run program

## Prod

``` bash
node index.js < leapfrog_ch_.txt &> output.txt
```

## Prod with example

``` bash
node index.js < leapfrog_ch__sample_input.txt &> output.txt
```

## Prod with fake file input

``` bash
OUTPUT=true STRESS=true node fakedata.js > fakeinput.txt && node index.js < fakeinput.txt &> output.txt
```

## Dev enviroment with example input

``` bash
npm run dev
```

## Dev enviroment with fake data

``` bash
npm run dev:fake
```

## Running with nodemon enviroment prod

``` bash
nodemon -V node index.js
```

## Running with nodemon enviroment dev

``` bash
nodemon --exec "DEV=true node" ./index.js
```

## Running with nodemon enviroment dev and fake data

``` bash
nodemon --exec "DEV=true FAKE=true node" ./index.js
```

## Running with nodemon enviroment dev and fake data

``` bash
nodemon --exec "DEV=true FAKE=true STRESS=true node" ./index.js
```

## Generate fake input

```bash
npm run dev:fake fakedata.js
```

## Generate fake input and output save to file

```bash
OUTPUT=true node fakedata.js > fakeinput.txt
```

## Generate fake input stress and output save to file

```bash
OUTPUT=true STRESS=true node fakedata.js > fakeinput.txt
```

## References

1. https://stackoverflow.com/questions/36540996/how-to-take-two-consecutive-input-with-the-readline-module-of-node-js
1. https://nodejs.org/api/readline.html#readline_rl_question_query_callback
1. https://github.com/remy/nodemon
1. https://www.valentinog.com/blog/memory-usage-node-js/
1. https://blog.abelotech.com/posts/measure-execution-time-nodejs-javascript/
1. https://medium.com/the-node-js-collection/making-your-node-js-work-everywhere-with-environment-variables-2da8cdf6e786
1. https://stackoverflow.com/questions/49169980/how-to-pipe-to-function-in-node-js
