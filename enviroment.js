const DEBUG = process.env.DEV || false
const INTERATIVE = process.env.INTERATIVE || false
const FAKE = process.env.FAKE || false
const CONSOLE = process.env.CONSOLE || false

function log () {
  if (!DEBUG) return
  args = Array.prototype.slice.call(arguments)
  console.info.apply(console, args)
}

module.exports = {
  log: log,
  DEBUG: DEBUG,
  INTERATIVE: INTERATIVE,
  FAKE: FAKE,
  CONSOLE: CONSOLE
}
