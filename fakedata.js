const faker = require('faker')
const OUTPUT = process.env.OUTPUT || false
const MAX_LINES = process.env.STRESS ? 500 : 8
const MAX_LILYPADS = process.env.STRESS ? 5000 : 13
const totalLines = faker.random.number({
  min: 2,
  max: MAX_LINES
})

!OUTPUT || process.stdout.write(totalLines.toString() + '\n')

const staticCharacters = [ 'B', '.' ]
let lines = [ totalLines ]
for (let i = 0; i < totalLines ; i ++) {
  const totalLilypads = faker.random.number({
    min: 1,
    max: MAX_LILYPADS
  })
  let lilypads = 'A'
  for (let lilypad = 0; lilypad < totalLilypads; lilypad ++) {
    let position = faker.random.boolean() ? 1 : 0
    let character = staticCharacters[position]
    lilypads += character
  }
  lines.push(lilypads)
}
if (OUTPUT) {
  process.stdout.write(lines.slice(1).join('\n') + '\n')
  process.exit()
}
module.exports = lines.join('\n')
// console.log(lines.join('\n'))
// print all lines
// process.stdout.write(lines.slice(1).join('\n') + '\n')
