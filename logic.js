const log = require('./enviroment').log

function runCase (caseLilypads) {
  log('run case: ', caseLilypads)
  const lilypads = caseLilypads.substr(1)
  if (lilypads.length === 1) {
    log('N')
    return 'N'
  }
  const lilypadsBetas = lilypads.split('B')
  if (lilypads.length === lilypadsBetas.length - 1) {
    log('N')
    return 'N'
  }
  const spaces = lilypadsBetas.join('').split('.').length - 1
  if (spaces === 1) {
    log('Y')
    return 'Y'
  }
  if (spaces === lilypadsBetas.length - 1) {
    log('Y')
    return 'Y'
  }
  if (spaces % 2 !== 0) {
    log('N')
    return 'N'
  }
  if (spaces === (lilypadsBetas.length / 2)) {
    log('Y')
    return 'Y'
  }
  log('Y')
  return 'Y'
}

module.exports = runCase
