const fs = require('fs')
const data = fs.readFileSync(0, 'utf-8')

module.exports = (logic) => {
  const lines = data.split('\n')
  const totalQuestions = parseInt(lines[0]) + 1
  let result = ''
  for (let i = 1; i < totalQuestions; i++) {
    result += `Case #${i}: ${logic(lines[i])}\n`
  }
  return result
}
